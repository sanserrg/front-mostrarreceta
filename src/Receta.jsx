import React, { useState, useEffect } from "react";
import { Container, Row, Col } from 'reactstrap';
import blanco from './img/blanco.svg';
import rojo from './img/rojo.svg';

import "./estilos.css";

const Receta = () => {
    const [receta, setReceta] = useState({});
    const [yummies, setYummies] = useState();
    const [ingredientes, setIngredientes] = useState([{}]);
    const [pasos, setPasos] = useState([{}]);
    const [comentarios, setComentarios] = useState([{}]);
    const [like, setLike] = useState(false);

    const recetaId = "5fc11a179b8a498f05088eec";
    const api_url = "http://localhost:8080/api/receta";

    const getById = (recetaId) => {
        const promesa = (res, rej) => {
            fetch(api_url + '/' + recetaId)
                .then(data => data.json())
                .then(receta => {
                    receta.id = receta._id;
                    res(receta);
                })
                .catch(err => {
                    rej(err);
                });
        };
        return new Promise(promesa);
    }

    useEffect(() => {
        getById(recetaId)
            .then(data => {
                setReceta(data)
                setYummies(data.yummies)
                setIngredientes(data.ingredientes)
                setPasos(data.pasos)
                setComentarios(data.comentarios)
            })
            .catch(err => {
                // return <Redirect to="/..." />;
            });
    }, [recetaId])

    const meGusta = () => {
        setLike(!like);
    }

    useEffect(() => {
        like ? setYummies(yummies + 1) : setYummies(0);
    }, [like])

    return (
        <>
            <Container>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <h1>{receta.nombre}</h1>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="divfoto">
                            <img className="foto" src={receta.foto} alt={receta.nombre} />
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={6} sm={8} lg={6}>
                        <div className="centrado">
                            <h5>{receta.tiempo + " minutos"}</h5>
                        </div>
                    </Col>
                    <Col xs={6} sm={8} lg={6}>
                        <div className="centrado">
                            <img className="icon" src={like ? rojo : blanco} onClick={meGusta}></img>
                            <h8 className="yummies">{yummies}</h8>
                        </div>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            <h4>{"Ingredientes"}</h4>
                        </div>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            {ingredientes.map((e, idx) => (
                                <div key={idx}>
                                    <h7>{e.nombre} ({e.cantidad} {e.unidad})</h7>
                                </div>
                            ))}
                        </div>
                    </Col>
                </Row>
                <Row>
                    <br />
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            <h4>{"Pasos"}</h4>
                        </div>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            {pasos.map((e, idx) => (
                                <div key={idx}>
                                    <br />
                                    <h7>{idx + 1}- {e.paso}</h7>
                                </div>
                            ))}
                        </div>
                    </Col>
                </Row>
                <br />
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            <h4>{"Comentarios"}</h4>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <div>
                        {comentarios.map((e, idx) => (
                            <Col key={idx} xs={12} sm={8} md={12} lg={12}>
                                <div className="comment">
                                    <br />
                                    <h7>{e.comentario}</h7>
                                </div>
                            </Col>
                        ))}
                    </div>
                </Row>
            </Container>
        </>
    );
};

export default Receta;